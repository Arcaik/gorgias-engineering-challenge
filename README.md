# Gorgias Engineering Challenge

[TOC]

## Overview

This repository contains the code of a simple ToDo application that uses
PostgreSQL as a backend and the manifests to deploy it in Kubernetes.

The directory structure of the repository is as follow (explained in more
details in the following sections):

```
.
├── kubernetes
│   ├── helm-charts                   # Directory for Helm Charts
│   └── manifests
│       └── <namespace>               # Directory for namespaced resources
│           ├── <application>         # Directory for application specific resources
│           │   ├── release.yaml      # Helm release values
│           │   ├── values.sops.yaml  # Secret values passed to the helm release
│           │   ├── foo.yaml
│           │   └── bar.yaml
│           ├── namespace.yaml        # Namespace definition
│           ├── foo.yaml
│           └── bar.yaml
├── terraform                         # The terraform code
└── todo                              # Source code of the ToDo webapp
```

The Kubernetes cluster is deployed using Terraform and manifests/helm-charts
are synchronized using Flux. The following diagram gives an overview of how the
services interact together:

![Infrastructure Overview Diagram](docs/images/infrastructure-overview.png)

## ToDo webapp

The ToDo application provides a ToDo list view with the possibility to append
new items.

Even if the challenge description suggested using Flask, I chose to use Django
to create the webapp as this is the framework I’m more familiar with. This is a
trade off that I think still allows me to showcase my Python skills while
preventing me from spending too much time on the development part.

A container image is [available on Docker Hub][docker-hub-arcaik-todo].

To build the container image locally, simply run the following command inside
the `todo/` directory:

```
$ docker build -t todo:<version> .
```

The application can be run locally using a non-persistent SQLite database as
such:

```
$ docker run --rm -it --read-only -u $(id -u nobody):$(id -g nobody) --tmpfs /tmp --tmpfs /app/static -e SECRET_KEY="dummy" -e ENVIRONMENT="local-dev" -e DB_PATH=/tmp/db.sqlite -p 8000:8000 todo:<version>
```

The web service should then be available at http://127.0.0.1:8000.

[docker-hub-arcaik-todo]: https://hub.docker.com/r/arcaik/todo

### Real Life Scenario

In a real life scenario I would do a multi-stage build and use
[distroless][distroless] as base image to reduce the attack surface on the
container.

The current image also doesn’t expose static files (not needed in this case)
and it would require an NGINX sidecar container to do so.

[distroless]: https://github.com/GoogleContainerTools/distroless

## Terraform

Terraform was used to setup the GKE cluster and its state is stored in the
`gorgias-terraform-states-ca4dbde7b54b` GCS bucket. Changes can be applied
using `terraform plan` and `terraform apply` from the `terraform/` directory.

### Real Life Scenario

All the settings (cluster location, type of instances, size of boot disk, etc.)
have been hardcoded in the Terraform code, but in a real life scenario, I would
probably create terraform modules, with hardcoded default settings and
variables for settings that are project-specific.

Those modules could then be put together using tools like terragrunt for
instance.

## Kubernetes

Manifest and Helm releases are managed by [Flux][fluxcd]. Flux will synchronize
the cluster with the content of the `kubernetes/manifests/` directory.

I created a simple Helm Chart for the ToDo webapp in the
`kubernetes/helm-charts/` directory.

The secrets are encrypted with [sops][sops] using [age][age] as a backend. To
have access to an encrypted secret, one needs to generate a key through the
`age-keygen` tool store the private part in sops’ config directory (see [sops’
documentation][sops-doc] for details) and send me the public part of the key.

To deploy a new version of the webapp, simply edit image repository/tag in the
release file at `kubernetes/manifests/todo/todo/release.yaml`, i.e.:

```
---
apiVersion: helm.toolkit.fluxcd.io/v2beta1
kind: HelmRelease
metadata:
  name: todo
  namespace: todo
spec:
  [...]
  values:
    deployment:
      image:
        repository: gcr.io/gorgias/todo
        tag: 0.2.0
  [...]
```

It should take arround 5 minutes after this is commited/pushed for Flux to
catch it and for the application to be updated.

[fluxcd]: https://fluxcd.io/
[sops]: https://github.com/mozilla/sops
[age]: https://github.com/FiloSottile/age
[sops-doc]: https://github.com/mozilla/sops#encrypting-using-age

### Real Life Scenario

#### Secrets encryption

Secrets in this repository are encrypted using sops and age, which means
granting someone access to the secrets requires a re-encryption of them all.
This works for simple deployment but in an actual repository I would rather use
sops with KMS as backend or something like [Sealed Secrets][sealed-secrets]

[sealed-secrets]: https://github.com/bitnami-labs/sealed-secrets

#### Deployment

The current implementation of the ToDo webapp deployment uses a GCP Load
Balancer provisioned through a Service of type `LoadBalancer` (this is a TCP
Load Balancer that’ll forward TCP requests directly to the pods).

It would be better to use Ingress resources with the [NGINX Ingress
Controller][ingress-nginx] as it would provide more flexibility and better
configuration options (especially when it comes to authentication or header
manipulation).

[ingress-nginx]: https://github.com/kubernetes/ingress-nginx


## PostgreSQL

I deployed two instances of PostgreSQL:

* a *primary* StatefulSet that accepts read-write connections (only one Pod can
  exist) and
* a *replica* StatefulSet that do stream replication and accepts read-only
  connections (which can spawn multiple Pods).

The `replication` and `todo` users and the `todo` database were created by hand:

```
postgres=# CREATE ROLE replication WITH REPLICATION PASSWORD '<password>' LOGIN;
postgres=# CREATE ROLE todo PASSWORD '<password>' LOGIN;
postgres=# CREATE DATABASE todo OWNER todo;
```

### Real Life Scenario

While this was not allowed by the challenge’s rules, in a real life scenario I
would use a Kubernetes Operator (like [Zalando’s
postgres-operator][postgres-operator]) to deploy and manage such databases as
it would allow for a real HA setup instead of a simple replication.

[postgres-operator]: https://github.com/zalando/postgres-operator
