resource "google_project_service" "container" {
  service            = "container.googleapis.com"
  disable_on_destroy = "false"
}

resource "google_container_cluster" "gke" {
  name     = "gorgias-todo"
  location = "us-east1-b"

  initial_node_count = 3

  node_config {
    machine_type = "n1-standard-1"
    disk_size_gb = 20
  }
}
