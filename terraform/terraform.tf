terraform {
  required_version = "~> 1.1.0"

  required_providers {
    google = {
      source  = "google"
      version = "~> 4.14"
    }
  }

  backend "gcs" {
    bucket = "gorgias-terraform-states-ca4dbde7b54b"
  }
}
