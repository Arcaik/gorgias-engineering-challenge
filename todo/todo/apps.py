from django.apps import AppConfig


class AppConfig(AppConfig):
    name = "todo"
    default_auto_field = "django.db.models.BigAutoField"
