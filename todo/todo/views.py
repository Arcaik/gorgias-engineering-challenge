from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods

from . import models, forms


def page_not_found(request, exception):
    return render(request, "404.html")


def forbiden(request, exception):
    return render(request, "403.html")


def index(request):
    if request.method == "POST":
        form = forms.Todo(request.POST)

        if form.is_valid():
            form.save()
            return redirect("index")
    else:
        form = forms.Todo()

    return render(
        request, "index.html", {"form": form, "todos": models.Todo.objects.all()}
    )


@require_http_methods(["GET"])
def health(request):
    return HttpResponse(status=200)
