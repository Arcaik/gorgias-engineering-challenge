from django.urls import path

from . import views


handler403 = "todo.views.forbiden"
handler404 = "todo.views.page_not_found"

urlpatterns = [
    path("", views.index, name="index"),
    path("health/live", views.health),
    path("health/ready", views.health),
]
