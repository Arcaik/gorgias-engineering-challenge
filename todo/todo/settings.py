import os
import pathlib

from django.core.exceptions import ImproperlyConfigured
from django.contrib.messages import constants as messages


# Base settings

BASE_DIR = pathlib.Path(__file__).resolve().parent.parent

INSTALLED_APPS = [
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "todo",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

AUTHENTICATION_BACKENDS = []

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

ROOT_URLCONF = "todo.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "todo.wsgi.application"

LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True

MESSAGE_TAGS = {
    messages.DEBUG: "primary",
    messages.INFO: "info",
    messages.SUCCESS: "success",
    messages.WARNING: "warning",
    messages.ERROR: "danger",
}


# Runtime settings

RUNTIME_ENVIRONMENT = os.environ.get("ENVIRONMENT", "prod")

REQUIRED_ENV_VARS = ["SECRET_KEY"]

if RUNTIME_ENVIRONMENT != "local-dev":
    REQUIRED_ENV_VARS.extend(["DB_HOST", "DB_USER", "DB_PASSWORD", "DB_NAME"])

for env in REQUIRED_ENV_VARS:
    if env not in os.environ:
        raise ImproperlyConfigured(f"Required environment variable '{env}' is missing.")

BASE_PATH = os.environ.get("BASE_URL", "")

STATIC_ROOT = os.environ.get("STATIC_DIR", str(BASE_DIR / "static"))

SECRET_KEY = os.environ.get("SECRET_KEY")

ALLOWED_HOSTS = os.environ.get("ALLOWED_HOSTS", "*").split(" ")

DEBUG = os.environ.get("DEBUG", "False").lower() == "true"

STATIC_URL = str(pathlib.Path(BASE_PATH) / "static") + "/"

if RUNTIME_ENVIRONMENT != "local-dev":
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql",
            "HOST": os.environ.get("DB_HOST"),
            "PORT": os.environ.get("DB_PORT", ""),
            "NAME": os.environ.get("DB_NAME"),
            "USER": os.environ.get("DB_USER"),
            "PASSWORD": os.environ.get("DB_PASSWORD"),
        }
    }
else:
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.sqlite3",
            "NAME": os.environ.get("DB_PATH", "db.sqlite"),
        }
    }
