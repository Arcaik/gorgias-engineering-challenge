---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "todo.fullname" . }}
  labels:
    app.kubernetes.io/name: {{ template "todo.name" . }}
    app.kubernetes.io/instance: {{ .Release.Name }}
    app.kubernetes.io/managed-by: {{ .Release.Service }}
    helm.sh/chart: {{ template "todo.chart" . }}
spec:
  replicas: {{ .Values.deployment.replicas }}
  {{- with .Values.deployment.strategy }}
  strategy:
    {{- . | toYaml | nindent 4 }}
  {{- end }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ template "todo.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
  template:
    metadata:
      annotations:
        checksum.helm.sh/secret: {{ include (print $.Template.BasePath "/secret.yaml") . | sha256sum }}
        {{- with .Values.deployment.extraPodAnnotations }}
          {{- toYaml . | nindent 8 }}
        {{- end }}
      labels:
        app.kubernetes.io/name: {{ template "todo.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
        {{- with .Values.deployment.extraPodLabels }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
    spec:
      serviceAccountName: {{ template "todo.fullname" . }}
      {{- with .Values.deployment.priorityClassName }}
      priorityClassName: {{ . }}
      {{- end }}
      {{- if .Values.deployment.podSecurityContext }}
      securityContext:
        runAsUser: {{ .Values.deployment.podSecurityContext.runAsUser }}
        runAsGroup: {{ .Values.deployment.podSecurityContext.runAsGroup }}
        fsGroup: {{ .Values.deployment.podSecurityContext.fsGroup }}
      {{- end }}
      containers:
        - name: todo
          image: "{{ .Values.deployment.image.repository }}:{{ .Values.deployment.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.deployment.image.pullPolicy }}
          {{- with .Values.deployment.extraEnv }}
          env:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          envFrom:
            - secretRef:
                name: {{ template "todo.fullname" . }}-environment
          volumeMounts:
            - name: tmp
              mountPath: /tmp
              subPath: tmp/
            - name: tmp
              mountPath: /static/
              subPath: static/
          ports:
            - name: http
              protocol: TCP
              containerPort: 8000
          securityContext:
            readOnlyRootFilesystem: true
            capabilities:
              drop:
                - ALL
          {{- with .Values.deployment.resources }}
          resources:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          livenessProbe:
            httpGet:
              scheme: HTTP
              path: /
              port: http
            initialDelaySeconds: 10
            periodSeconds: 10
            timeoutSeconds: 1
            successThreshold: 1
            failureThreshold: 3
          readinessProbe:
            httpGet:
              scheme: HTTP
              path: /
              port: http
            initialDelaySeconds: 10
            periodSeconds: 10
            timeoutSeconds: 1
            successThreshold: 1
            failureThreshold: 3
      volumes:
        - name: tmp
          emptyDir: {}
      {{- with .Values.deployment.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.deployment.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.deployment.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
